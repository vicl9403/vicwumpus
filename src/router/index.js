import Vue from 'vue'
import Router from 'vue-router'
import MainComponent from '@/components/Main'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: MainComponent
    }
  ],
  mode: 'history'
})
